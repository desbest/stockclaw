# Stockclaw

Manage quantities of stock within categories, with historical reports

Stockclaw is a simple stock managing script. It got made because I was lazy and hadn't setup my work experience at the blind school so when my teacher found out, she got the school technicians to give me some useless work to do that they didn't want.

With Stockclaw you can store items with their quantity, in categories. Also a historical record is taken so you can go back in time to see how much stock of what items you had in the past.

MIT License

![stockclaw dashboard](https://i.imgur.com/sl2Dayw.png)
![stockclaw category](https://i.imgur.com/8fPFDgp.png)
![stockclaw history](https://i.imgur.com/slDaFmB.png)