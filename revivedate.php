<?php
$lang = 'en'; setcookie('lang', $lang);
// put here the correct path to "calendar.php"; don't move the file
// "calendar.php" -- I think it's best if you leave it inside the
// "/jscalendar/" directory.  Just put here the correct path to it, such as
// "../jscalendar/calendar.php" or something.
require_once ('jscalendar/calendar.php');

// parameters to constructor:
//     1. the absolute URL path to the calendar files
//     2. the languate used for the calendar (see the lang/ dir)
//     3. the theme file used for the clanedar, without the ".css" extension
//     4. boolean that specifies if the "_stripped" files are to be loaded
//        The stripped files are smaller as they have no whitespace and comments
$calendar = new DHTML_Calendar('jscalendar/', $lang, 'calendar-blue', false);

// call this in the <head> section; it will "echo" code that loads the calendar
// scripts and theme file.
$calendar->load_files();
?>
<html>
<head>

<title>Stock Manager</title>

<link rel="stylesheet" href="stylesheet.css" type="text/css">
<link rel="stylesheet" href="1kbgrid.css" type="text/css">
<link rel="stylesheet" href="alerts/jalert.css" type="text/css">
<link rel="stylesheet" href="cupertino/jquery-ui-1.8.1.custom.css" type="text/css">

<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="ui-core.js"></script>
<script type="text/javascript" src="jeditable.js"></script>
<script type="text/javascript" src="editabletext.js"></script>

<script type="text/javascript" src="hide.js"></script>
<script type="text/javascript" src="alerts/jalert.js"></script>

<script type="text/javascript" charset="utf-8">

 jQuery(function($){
     $('.editable').editableText({
          // default value
          newlinesEnabled: false
     });

   //  bind an event listener that will be called when
   //  user saves changed content
   $('.editable').change(function(){
      var newValue = $(this).html();
      // new value

      var itemid = $(this).attr("itemid");
      //$("div").text(title);
      //get the itemid
      var dataString = "itemid=" + itemid + "&newvalue=" + newValue;
      
      $.ajax({
         type: "POST",
         url: "formcontrols.php",
         data: dataString,
         success: function(msg){
         //alert(dataString);
         }
      });
   });
   
   
});
 </script>



</head>
<body>
<?php
require ("config.php");
?>


<div id="navcontainer">
   
<ul id="navlist">

<li id="active"><a href="index.php">Dashboard</a></li>
<li><a href="recentchanges.php">Recent changes</a></li>
<li><a href="history.php"  id="current">History</a></li>
<li><a href="../">Return to Lordswood Girls CP</a></li>
<li><a href="logout.php">Logout</a></li>
</ul>
</div>

<div class="header">History &raquo; Revive</div>
 <!--[if lt IE 7]>  <div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;'>    <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'><a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-cornerx.jpg' style='border: none;' alt='Close this notice'/></a></div>    <div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>      <div style='width: 75px; float: left;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-warning.jpg' alt='Warning!'/></div>      <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>        <div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>You are using an outdated browser</div>        <div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>For a better experience using this site, please upgrade to a modern web browser.</div>      </div>      <div style='width: 75px; float: left;'><a href='http://www.firefox.com' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox 3.5'/></a></div>      <div style='width: 75px; float: left;'><a href='http://www.browserforthebetter.com/download.html' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-ie8.jpg' style='border: none;' alt='Get Internet Explorer 8'/></a></div>      <div style='width: 73px; float: left;'><a href='http://www.apple.com/safari/download/' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari 4'/></a></div>      <div style='float: left;'><a href='http://www.google.com/chrome' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>    </div>  </div>  <![endif]-->


	<div class="offset">
	 <!-- close row -->
      
      <div class="row"><!-- open row and loggedin -->

      <div class="column grid_12">
      
      </div>
      
      <div class="column grid_4" >
         <div id="navcontainer">
         <ul id="navlist">

         <li id="active"><a></a></li></ul></div>
        

         <div class="iconbar">
         </div>
         
         
         </div> <!-- close column 4 -->

      
      
      <div class="column grid_8" >
         <div id="navcontainer">
			<ul id="navlist">
         <li id="active"><a>Revive Date</a></li></ul></div>
         
         <?php 
         if (isset($_POST['date1'])){ $thenewdate = $_POST['date1']; } else { $thenewdate = NULL; }
         $thenewdate = str_replace(",", "", "$thenewdate");

         $year = date('Y',strtotime($thenewdate));
         $monthname = date('F',strtotime($thenewdate));
         $dayname = date('l',strtotime($thenewdate));
         $daynumber = date('m',strtotime($thenewdate));
         $datenumber = date('Y-m-d',strtotime($thenewdate));
         $yearmonthnumber = date('Y-m',strtotime($thenewdate));

         $hour = date('H');
         $minutes = date('i');
         $time = $hour.":".$minutes;
         $datetimenumber = $datenumber."-".$time;
         $second = date('s');
         $datetimesecondnumber = $datetimenumber."-".$second;
         //date time enabled?
         ?>
         <?php
         $revivedalready = mysql_query("SELECT * FROM history_items WHERE datenumber ='$datenumber' ");
         $revivedalready = mysql_num_rows($revivedalready);
         if ($revivedalready == 0){
            echo "
            <div class=\"listingdark\" id=\"sel3\">
            All the items from <b>$datenumber</b> are getting revived. Please wait.
            <img src=\"icons/loading.gif\">
            </div>
            <table width=\"100%\" class=\"stylish\" style=\"border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;\" id=\"itemsshowhere\" celllpadding=\"0\" cellspacing=\"0\">
            <thead><tr>
            <th>Title</th>
            <th>Exact Date exists?</th>
            <th width=\"150\">Previous date exits?</th>
            <th width=\"150\">No record exists?</th>
            </tr></thead><tbody>
            ";
            $fetchhistory = mysql_query("SELECT * FROM history_items WHERE datenumber <= '$datenumber' ORDER BY datenumber DESC  ");
            while ($history = mysql_fetch_array($fetchhistory)){
               $fetchcategory = mysql_query("SELECT * FROM categories WHERE id='$history[categoryid]'");
               $category = mysql_fetch_array($fetchcategory);

               echo "
                     <tr class=\"sel2\">
                     <td height=\"30\">
                     <!-- <img src=\"move.png\" align=\"left\" style=\"margin-right: 8px;\"> -->
                     <div class=\"lowlight\">$history[title]                

                     </div>
                     </td>
                     <td height=\"30\" align=\"center\"> ";
                     
                     if ($history['datenumber'] == $datenumber){
                        echo "<img src=\"icons/tick.png\">";
                           mysql_query("
                           INSERT INTO history_items(title, datenumber, datetimenumber, datetimesecondnumber, yearmonthnumber, categoryid, quantity, mod_datenumber, mod_yearmonthnumber) 
                           VALUES ('$history[itemname]','$datenumber','$datetimenumber','$datetimesecondnumber','$yearmonthnumber','$history[categoryid]','$history[quantity]','$datenumber','$yearmonthnumber')
                     ");

                        }
                    
                     echo "
                     </td>
                     <td height=\"30\" align=\"center\"> ";

                     if ($history[datenumber] < $datenumber){
                        echo "<img src=\"icons/tick.png\">";
                           mysql_query("
                           INSERT INTO history_items(title, datenumber, datetimenumber, datetimesecondnumber, yearmonthnumber, categoryid, quantity, mod_datenumber, mod_yearmonthnumber) 
                           VALUES ('$history[title]','$datenumber','$datetimenumber','$datetimesecondnumber','$yearmonthnumber','$history[categoryid]','$history[quantity]','$datenumber','$yearmonthnumber')
                     ");

                     }


                     echo "</td>
                     <td align=\"center\">";
                     if ($history[datenumber] > $datenumber && $datenumber != $history[datenumber]){
                        echo "<img src=\"icons/tick.png\">";
                     }
                     echo" </td>
                     </tr>
                  ";
                  
               }
               
               $fetchitems = mysql_query("SELECT * FROM items");
               while ($item = mysql_fetch_array($fetchitems)){
                  if ($item['datenumber'] < $datenumber){
                     echo "
                     <tr class=\"sel2\">
                     <td>$item[title]</td>
                     <td></td>
                     <td></td>
                     <td height=\"30\" align=\"center\"><img src=\"icons/cross.png\"></td>
                     </td></tr>
                     ";
                  }
               }
               
            echo "</tbody></table>
            <div class=\"listingdark\">
            <script type=\"text/javascript\">
            $(document).ready(function(){
            $(\"#rev1\").hide();
            $(\"#sel3\").fadeOut(800);
            });
            </script>
            <form method=\"GET\" action=\"snapshot.php\">
            <input type=\"hidden\" name=\"date\" value=\"$datenumber\">
            <input type=\"submit\" value=\"View snapshot\" class=\"bigbutton\">
            </form>

            </div>
            ";

         } else {
            echo "<div class=\"listingdark\">
               `  <img src=\"icons/warning.png\" align=\"left\"><br>The date <b>$datenumber</b> has already been revived.
                  <br><a href=\"history.php\">Go back and select another date.
                  <br><br><br>
                  </div>
                  ";
         }
         ?>
         </tbody></table>



         <div class="listingdark" style="border-top: 0px solid black;">Stock Control is created by <a href="http://desbest.com" target="_blank">desbest</a>. Checkout the scripts for newer versions.</div>
         <div class="iconbar">
         </div>


         </div>

         <!-- end row -->

   <!-- /////// end content -->









 <br>
 
</div>
<div style="clear:  both;"></div>
<!-- close offset -->
<br><br><div class="footer">Powered by Stock Manager v4</div>

</body>

</html>