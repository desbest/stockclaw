<?php
ob_Start;
setcookie("userid", 'loggedout',time()+(60*60*24*5));
setcookie("pass", 'loggedout',time()+(60*60*24*5));
?>
<html>
<head>
<title>Stock Manager</title>

<link rel="stylesheet" href="stylesheet.css" type="text/css">
<link rel="stylesheet" href="1kbgrid.css" type="text/css">
<link rel="stylesheet" href="alerts/jalert.css" type="text/css">
<link rel="stylesheet" href="cupertino/jquery-ui-1.8.1.custom.css" type="text/css">

<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="ui-core.js"></script>
<script type="text/javascript" src="jeditable.js"></script>

<script type="text/javascript" src="editabletext.js"></script>
<script type="text/javascript" src="hide.js"></script>
<script type="text/javascript" src="alerts/jalert.js"></script>

</head>
<body>

 <!--[if lt IE 7]>  <div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;'>    <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'><a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-cornerx.jpg' style='border: none;' alt='Close this notice'/></a></div>    <div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>      <div style='width: 75px; float: left;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-warning.jpg' alt='Warning!'/></div>      <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>        <div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>You are using an outdated browser</div>        <div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>For a better experience using this site, please upgrade to a modern web browser.</div>      </div>      <div style='width: 75px; float: left;'><a href='http://www.firefox.com' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox 3.5'/></a></div>      <div style='width: 75px; float: left;'><a href='http://www.browserforthebetter.com/download.html' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-ie8.jpg' style='border: none;' alt='Get Internet Explorer 8'/></a></div>      <div style='width: 73px; float: left;'><a href='http://www.apple.com/safari/download/' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari 4'/></a></div>      <div style='float: left;'><a href='http://www.google.com/chrome' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>    </div>  </div>  <![endif]-->


<body> 



<div id="navcontainer">
<ul id="navlist">
<!-- <li><a href="../">Return to Lordswood Girls CP</a></li> -->
</ul>
</div>
<div class="header">Stock Control &raquo; Logout   </div>

<div class="offset">


<div class="row">

	<div class="column grid_12">
	<div class="notice">You have been logged out.</div>
	<div class="listinglight">Stock Control is created by <a href="http://desbest.com" target="_blank">desbest</a>. Contact him to get Stock Control v2.</div>
	<div class="listingdark"><a href="index.php">Return to Stock Control.</a></div>
	</div>
   
	<div class="column grid_4" >
   </div>
         
	<div class="column grid_8">
   </div>
   </div> <!-- close row -->
               

 <br>
 
<br><br>
<div style="clear:  both;"></div>
<!-- close offset -->
<br><br><div class="footer">Powered by Stockclaw v1.0.5</div>

</body>

</html>