<html>
<head>
<title>Stock Manager</title>

<link rel="stylesheet" href="stylesheet.css" type="text/css">
<link rel="stylesheet" href="1kbgrid.css" type="text/css">
<link rel="stylesheet" href="alerts/jalert.css" type="text/css">
<link rel="stylesheet" href="cupertino/jquery-ui-1.8.1.custom.css" type="text/css">

<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="ui-core.js"></script>
<script type="text/javascript" src="jeditable.js"></script>
<script type="text/javascript" src="editabletext.js"></script>

<script type="text/javascript" src="hide.js"></script>
<script type="text/javascript" src="alerts/jalert.js"></script>

<script type="text/javascript" charset="utf-8">

 jQuery(function($){
     $('.editable').editableText({
          // default value
          newlinesEnabled: false
     });

   //  bind an event listener that will be called when
   //  user saves changed content
   $('.editable').change(function(){
      var newValue = $(this).html();
      // new value

      var itemid = $(this).attr("itemid");
      //$("div").text(title);
      //get the itemid
      var dataString = "itemid=" + itemid + "&newvalue=" + newValue;
      
      $.ajax({
         type: "POST",
         url: "formcontrols.php",
         data: dataString,
         success: function(msg){
         //alert(dataString);
         }
      });
   });
   
   
});
 </script>



</head>
<body>
<?php
require ("config.php");
?>


<div id="navcontainer">
   
<ul id="navlist">

<li id="active"><a href="index.php">Dashboard</a></li>
<li><a href="recentchanges.php"  id="current">Recent changes</a></li>
<li><a href="history.php">History</a></li>
<!-- <li><a href="../">Return to Lordswood Girls CP</a></li> -->
<li><a href="logout.php">Logout</a></li>
</ul>
</div>

<div class="header">Recent Changes</div>
 <!--[if lt IE 7]>  <div style='border: 1px solid #F7941D; background: #FEEFDA; text-align: center; clear: both; height: 75px; position: relative;'>    <div style='position: absolute; right: 3px; top: 3px; font-family: courier new; font-weight: bold;'><a href='#' onclick='javascript:this.parentNode.parentNode.style.display="none"; return false;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-cornerx.jpg' style='border: none;' alt='Close this notice'/></a></div>    <div style='width: 640px; margin: 0 auto; text-align: left; padding: 0; overflow: hidden; color: black;'>      <div style='width: 75px; float: left;'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-warning.jpg' alt='Warning!'/></div>      <div style='width: 275px; float: left; font-family: Arial, sans-serif;'>        <div style='font-size: 14px; font-weight: bold; margin-top: 12px;'>You are using an outdated browser</div>        <div style='font-size: 12px; margin-top: 6px; line-height: 12px;'>For a better experience using this site, please upgrade to a modern web browser.</div>      </div>      <div style='width: 75px; float: left;'><a href='http://www.firefox.com' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-firefox.jpg' style='border: none;' alt='Get Firefox 3.5'/></a></div>      <div style='width: 75px; float: left;'><a href='http://www.browserforthebetter.com/download.html' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-ie8.jpg' style='border: none;' alt='Get Internet Explorer 8'/></a></div>      <div style='width: 73px; float: left;'><a href='http://www.apple.com/safari/download/' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-safari.jpg' style='border: none;' alt='Get Safari 4'/></a></div>      <div style='float: left;'><a href='http://www.google.com/chrome' target='_blank'><img src='http://www.ie6nomore.com/files/theme/ie6nomore-chrome.jpg' style='border: none;' alt='Get Google Chrome'/></a></div>    </div>  </div>  <![endif]-->


	<div class="offset">
	 <!-- close row -->
      
      <div class="row"><!-- open row and loggedin -->

      <div class="column grid_12">
      
      </div>
      
      <div class="column grid_4" >
         <div id="navcontainer">
         <ul id="navlist">

         <li id="active"><a>Time Scale</a></li></ul></div>
         <div class="listinglight">
         <?php if (isset($_GET['timescale']) && $_GET['timescale'] == "thismonth"){ $theclass = "active"; } else { $theclass = ""; }         ?>
            <a href="recentchanges.php?timescale=thismonth" class="<?php echo "$theclass"; ?>">     
               This Month</font></a>
            </a>
         </div><div class="listinglight">
                  <?php if (isset($_GET['timescale']) && $_GET['timescale'] == "lastmonth"){ $theclass = "active"; } else { $theclass = ""; }         ?>

            <a href="recentchanges.php?timescale=lastmonth" class="<?php echo "$theclass"; ?>">Last Month</a>
         </div>
        

         <div class="iconbar">
         </div>
         
         
         </div> <!-- close column 4 -->

      
      
      <div class="column grid_8" >
         <div id="navcontainer">
			<ul id="navlist">
         <li id="active"><a>Recent Changes</a></li></ul></div>
         
        <?php 
        if (!isset($_GET['timescale'])){
        ?>
         <div class="listingdark">Each time an item is added, gets its quantity changed or is moved to another category, it is classed as modified or as better known, changed.. 
         <br>
         <br>Now is your chance to see the recent changes. Choose a link on the left to get started.
         </div>
         <div class="listingdark">Note that recent changes does not show a snapshot of the stock the items were at an earlier date. That feature called History, is in version 1.0.2.</div>

         <div class="listingdark" style="border-top: 0px solid black;">Stock Control is created by <a href="http://desbest.uk.to" target="_blank">desbest</a>. Checkout the scripts for newer versions.</div>
       <?php 
        } else {
        $yearmonthnumber = date("Y-m");
        
         $today = date("Y-m-d");
         $lastmonth = strtotime ( '-1 month' , strtotime ( $today ) ) ;
         $lastmonth = date ( 'Y-m' , $lastmonth );

        if ($_GET[timescale] == "thismonth" || $_GET[timescale] == "lastmonth"){
        
        if ($_GET[timescale] == "thismonth"){
            $thismonth = mysql_query("SELECT * FROM items WHERE yearmonthnumber='$yearmonthnumber'") or die(mysql_error());
        } else {
              $thismonth = mysql_query("SELECT * FROM items WHERE yearmonthnumber='$lastmonth'") or die(mysql_error());
        }
        echo "
                             <table width=\"100%\" class=\"stylish\" id=\"itemsshowhere\" celllpadding=\"0\" cellspacing=\"0\">
         <thead><tr>
                           <th>Title</th>
                           <th>Quantity</th>
                           <th width=\"150\">Category</th>
                        </tr></thead><tbody>

         ";
         while($modified = mysql_fetch_array($thismonth))
         {
           $fetchcat = mysql_query("SELECT * FROM categories WHERE id='$modified[categoryid]'") or die(mysql_error());
           $gotcat = mysql_fetch_array($fetchcat);

            echo "
         <tr id=\"itemid_[items.id;block=tr]\">
                    <td height=\"30\">
                    <!-- <img src=\"move.png\" align=\"left\" style=\"margin-right: 8px;\"> -->
                    <div class=\"lowlight\">$modified[title]
                    
                    
                    
                    </div>
                    

                    </td>
                    <td height=\"30\"> 
                    <div class=\"editable\" itemid=\"$modified[id]\">$modified[quantity]</div>

                    </td>
                    <td height=\"30\"> 
                     <a href=\"index.php?category=$modified[categoryid]\">$gotcat[title]</a>
                    </td>
        </tr> 
        

            ";
         }
         echo "        </tbody></table>";
         
         
       }
       ?>
 
         
       <?php
       }
       ?>  
         <div class="iconbar">
         </div>

         
      </div>

    <!-- end row -->
   
   <!-- /////// end content -->









 <br>
 
</div>
<br><br>
<div style="clear:  both;"></div>
<!-- close offset -->
<br><br><div class="footer">Powered by Stock Manager v1.0.5</div>

</body>

</html>