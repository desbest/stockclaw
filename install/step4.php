<html>
<head>
<title>Stock Manager Installer</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#loading").hide();    
   $("#hideonclick").click(function (){
         $("#hideonclick").hide();    
         $("#loading").show();    
   });
});
</script>
</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li><a>Step 1</a></li>
<li><a>Step 2</a></li>
<li><a>Step 3</a></li>
<li id="active"><a id="current">Step 4</a></li>
<li><a>Step 5</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Install Stock Manager</font>
<?php
ob_start();
// allows you to use cookies
require ("../config.php");
//gets the config page
if ($_POST[register])
{
    // the above line checks to see if the html form has been submitted
    if ($dbusername == null | $accesshost == null | $database == null) 
    {
        //checks to make sure no fields were left blank
        echo "<img src=\"warning.png\">Your database configuration was incorrect.
		<br><a href=\"step2.php\">Go back</a>
		";
    }
    else
    {
        //none were left blank!  We continue...
        

$sql = array (
           0 => "
   -- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2010 at 12:02 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE=\"NO_AUTO_VALUE_ON_ZERO\";

--

         ",
         1 => " 

-- Database: `stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `parentid` varchar(255) NOT NULL DEFAULT '0',
  `items` varchar(255) NOT NULL DEFAULT '0',
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `datetimesecondnumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

         ",
         2 => " 

-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `parentid`, `items`, `datenumber`, `datetimenumber`, `datetimesecondnumber`) VALUES
(1, 'Uncategorised', '0', '0', '2010-05-07', '2010-05-07-12:13', '2010-05-07-12:13-06');

-- --------------------------------------------------------
         ",
         3 => " 

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `categoryid` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `datenumber` varchar(255) NOT NULL,
  `datetimenumber` varchar(255) NOT NULL,
  `datetimesecondnumber` varchar(255) NOT NULL,
  `yearmonthnumber` varchar(255) NOT NULL,
  
  `mod_datenumber` varchar(255) NOT NULL,
  `mod_yearmonthnumber` varchar(255) NOT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

         ",
         4 => " 

   -- --------------------------------------------------------

   --
   -- Table structure for table `resets`
   --

   CREATE TABLE IF NOT EXISTS `resets` (
     `id` bigint(255) NOT NULL AUTO_INCREMENT,
     `userid` varchar(255) NOT NULL,
     `datenumber` varchar(255) NOT NULL,
     `accesscode` varchar(255) NOT NULL,
     PRIMARY KEY (`id`)
   ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

         ",
         5 => "
-- Dumping data for table `resets`
--


-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `minimumstock` varchar(255) NOT NULL,
  `emailtoalert` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

         ",
         6 => "
               -- Dumping data for table `settings`
               --

               INSERT INTO `settings` (`id`, `minimumstock`, `emailtoalert`) VALUES
               (1, '2', 'do@not.reply');
         ",
         7 => " 

               -- --------------------------------------------------------

               --
               -- Table structure for table `users`
               --

               CREATE TABLE IF NOT EXISTS `users` (
                 `id` int(11) NOT NULL AUTO_INCREMENT,
                 `username` varchar(30) NOT NULL DEFAULT '',
                 `password` varchar(255) NOT NULL DEFAULT '',
                 `email` varchar(40) NOT NULL DEFAULT '',
                 `msn` varchar(250) NOT NULL DEFAULT 'Not Specified',
                 `aim` varchar(250) NOT NULL DEFAULT 'Not Specified',
                 `location` varchar(36) NOT NULL DEFAULT 'Not Specified',
                 `level` varchar(255) NOT NULL,
                 PRIMARY KEY (`id`)
               ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

         ",
         8 => "-- v3 onwards
               -- --------------------------------------------------------

               --
               -- Table structure for table `history_items`
               --

               CREATE TABLE IF NOT EXISTS `history_items` (
                 `id` bigint(255) NOT NULL AUTO_INCREMENT,
                 `title` varchar(255) NOT NULL,
                 `categoryid` varchar(255) NOT NULL,
                 `quantity` varchar(255) NOT NULL,
                 `datenumber` varchar(255) NOT NULL,
                 `datetimenumber` varchar(255) NOT NULL,
                 `datetimesecondnumber` varchar(255) NOT NULL,
                 `yearmonthnumber` varchar(255) NOT NULL,
                 
                 `mod_datenumber` varchar(255) NOT NULL,
                 `mod_yearmonthnumber` varchar(255) NOT NULL,
                  PRIMARY KEY (`id`)
               ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

         ",
         9 => "
               -- --------------------------------------------------------

               --
               -- Table structure for table `fields`
               --

                  CREATE TABLE IF NOT EXISTS `fields` (
                    `id` int(255) NOT NULL AUTO_INCREMENT,
                    `itemid` int(255) NOT NULL,
                    `attribute` varchar(255) NOT NULL,
                    `value` varchar(255) NOT NULL,
                    `datenumber` varchar(255) NOT NULL,
                    `datetimenumber` varchar(255) NOT NULL,
                    `datetimesecondnumber` varchar(255) NOT NULL,
                    `yearmonthnumber` varchar(255) NOT NULL,
                    `mod_datenumber` varchar(255) NOT NULL,
                    `mod_yearmonthnumber` varchar(255) NOT NULL,
                    PRIMARY KEY (`id`)
                  ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

         ",
         10 => "
               -- --------------------------------------------------------

               --
               -- Table structure for table `history_fields`
               --

                  CREATE TABLE IF NOT EXISTS `history_fields` (
                    `id` int(255) NOT NULL AUTO_INCREMENT,
                    `itemid` int(255) NOT NULL,
                    `attribute` varchar(255) NOT NULL,
                    `value` varchar(255) NOT NULL,
                    `datenumber` varchar(255) NOT NULL,
                    `datetimenumber` varchar(255) NOT NULL,
                    `datetimesecondnumber` varchar(255) NOT NULL,
                    `yearmonthnumber` varchar(255) NOT NULL,
                    `mod_datenumber` varchar(255) NOT NULL,
                    `mod_yearmonthnumber` varchar(255) NOT NULL,
                    PRIMARY KEY (`id`)
                  ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;
         
         "
         );




$dbhost = '';
@set_time_limit(0);
$db = mysql_connect($accesshost, $dbusername, $dbpassword);
if(!$db) die('Cannot connect: ' . mysql_error());
$res = mysql_select_db($database);
if(!$res) die('Cannot select database "' . $database . '": ' . mysql_error());
for($i=0; $i<count($sql); $i++)
{
 if($table_prefix !== 'phpbb_') $sql[$i] = preg_replace('/phpbb_/', $table_prefix, $sql[$i]);
 $res = mysql_query($sql[$i]);
 if(!$res) { echo '<hr><b>error in query ', ($i + 1), ': </b>', mysql_error(), ''; echo "<br><textarea cols=\"50\" class=\"mediumtext\" rows=\"16\">$sql[$i]</textarea>"; }
}
echo ("
<script type=\"text/javascript\">
   $(document).ready(function(){
      $(\"#loading\").hide();    
   });
</script>

<br><br>
");
         if ($res) {
         echo "<img src=\"tick.png\" align=\"left\">The database has been populated.
         <br><a href=\"step5.php\">Contiune to step 4</a>
         "; }if (!$res) {
         echo "<img src=\"warning.png\" align=\"left\">There were errors!
         <br><a href=\"http://desbest.uk.to\">Contact desbest from his website.</a>
         <br><a href=\"step5.php\">Continue anyway.</a>
         ";
         }
//echo 'done (', count($sql), ' queries).';

		
				
            }
        }
    
	
else
{
    // the form has not been submitted...so now we display it.
    
include ("../config.php");
    $tryme = mysql_query("SELECT * FROM users");  
    $tryme2 = mysql_query("SELECT * FROM items");  
    if ($tryme ||$tryme2){
    echo "<br><img src=\"warning.png\" align=\"left\">Stock Manager has been already installed.
    <br>Uninstall it before installing again by deleting the mysql database.
    <br><br>
    <br>If you are looking to upgrade Stock Control to a newer version, disregard that warning.
    <br>You should instead, <a href=\"step4upgrade.php\">choose the upgrade option.</a>
    ";
    } else{


    echo ("
<br>Click the button below ONCE.

<form method=\"POST\" >



<br>
<input name=\"register\" type=\"submit\" id=\"hideonclick\"class=\"bigbutton\" value=\"Populate Database Tables\">
<div id=\"loading\"><img src=\"loading.gif\"></div>
</form>
			
 "); 
 }
}






			
?>			  
			  



</div>			  
</body>
</html>