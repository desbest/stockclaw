<html>
<head>
<title>Stock Manager Installer</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li id="active"><a id="current">readme.txt</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Stock Manager Readme (readme.txt)</font>
<br>		
<textarea class="minitext" style="width: 600px; height: 300px;">=======================
Upgrading Instructions
>) Stock Manager
=======================

=======================
Upgrading to v4
=======================
===============================================================
1> Upload all the Stock Manager v4 files except for config.php, via ftp
   replacing your existing files with the newer v4 version files.
   
2> Go to install/ in order to install the script. You will be asked in the
   installation process whether you wish to install Stock Manager (which
   you cannot do as your databse isn't empty). Choose the upgrade option to
   update your existing Stock Manager installation to v4.
   
==============================================================
=======================
What's new in v3
=======================
|> Fields.
|> The only design glitches in Internet Explorer 8 are fixed.
   A wooden background being show by the uncategorised category once filled.
==============================================================
Known Bugs
=======================
|> None. If you find some, report them.      
==============================================================
=======================
MSNY License
=======================
Stock Manager is copyright of desbest and is provided under the MSNY 
license. This license is available as a file called license.txt in your stock
control directory.

Stock Control
v1> (c) desbest (2010)
v2> (c) desbest (2010)
v3> (c) desbest (2010)
v4> (c) desbest (2010)
==============================================================

==============================================================
===================================END========================
</textarea>
<br>
<br><a href="index.php">Return to the installer</a>
			  



</div>			  
</body>
</html>